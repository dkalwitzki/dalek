#include <HCSR04.h>

HCSR04 hc(2, new int[3]{3,4,5},3);//initialisation class HCSR04 (trig pin , echo pin)


void setup() {
  // put your setup code here, to run once:
pinMode(10, OUTPUT);
Serial.begin(9600);
}

void loop() {
  // put your main code here, to run repeatedly:
  int dist = 0;
for (int i = 0; i < 3; i++ ) {
  int local = 0;
  local = hc.dist(i);
  Serial.print( local );
  Serial.print("   ");
  if (local < dist || i == 0) {
    dist = local;
  }
  
  }
  Serial.println("");
if (dist<100) {
  digitalWrite(10, LOW);
} else {
  digitalWrite(10, HIGH);
}
}
