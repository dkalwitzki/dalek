void setup() {
   Serial.begin(19200);
   Serial2.begin(19200);
}

void loop() {
   while (Serial2.available()) {
    Serial.write(Serial2.read());
   }

   //Serial.print("X: "); Serial.println(analogRead(A2));
   //Serial.print("Y: "); Serial.println(analogRead(A3));
   int32_t vertical   = map(analogRead(A2)+200, 0, 4096,  0,  180);
   int32_t horizontal = map(analogRead(A3)+220, 0, 4096,  -50, 50);  //left to right
   int32_t left       = vertical + horizontal;
   int32_t right      = vertical - horizontal;

   left  = max(left, 0);
   left  = min(left, 180);
   right = max(right, 0);
   right = min(right, 180);

   if (abs(left-90) < 5){
    left = 90;
   }

   if (abs(right-90) < 5){
    right = 90;
   }

   String a = "/motorControl?";
   a+=left;
   a+="=";
   a+=right;   
   
   Serial.println(a);
   Serial2.println(a);
   delay(300);
}     