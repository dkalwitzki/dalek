#ifndef PINOUT_H
#define PINOUT_H

#define PIN_MOTOR_PWM_LEFT     10 // PWM
#define PIN_MOTOR_PWM_RIGHT    11 // PWM
#define PIN_MOTOR_BREAK        9


#define PIN_BATTERY           A0

#endif
