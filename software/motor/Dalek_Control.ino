
#include <SoftwareSerial.h>
#include <aREST.h>
#include "pinout.h"
#include "motor.h"

SoftwareSerial Serial1S(2,3);

aREST rest = aREST();

void setup() {
  // put your setup code here, to run once:
  Serial1S.begin(19200);
  motor_setup();
  rest.function((char*)"motorControl", motorControl);
}

int motorControl(String command)
{
  Serial.println(command);
  if (command.indexOf("=") != -1)
  {
    int id = 0;
    int value = 0;

    char delimiter[] = "=";
    char stringBuffer[command.length() + 1];
    command.toCharArray(stringBuffer, command.length() + 1);

    char *ptr = strtok(stringBuffer, delimiter);

    while (ptr != NULL)
    {
      if (0 != id)
      {
        value = atoi(ptr);
      }
      else
      {
        id = atoi(ptr);
      }
      ptr = strtok(NULL, delimiter);
    }

    motor_speed(id,value);
    return 1;
  }
  else
  {
    return 0;
  }
}

void loop() {
  rest.handle(Serial1S);
  motor_control();
  for (int i = 0 ; i < 100; i++) {
    motor_control();
    delay(10);
    i++;
  }
}
